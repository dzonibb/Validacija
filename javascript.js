$(document).ready(function () {
    $("button").click(function () {

        var datumOd = {};
        var datumDo = {};

        $.each($('#form').serializeArray(), function (i, field) {
            var valid = validate(field);

            if (valid) {
                if (i < 3) {
                    datumOd[field.name] = field.value;
                }
                else {
                    datumDo[field.name] = field.value;
                }
            }
            else
                return false;
        });

        function validate(field) {
            
            if (field.name == 'dan-od' || field.name == 'dan-do') {
                if (field.value.length != 2) {
                    $("span").text("Dan bi trebao da sadrzi dva broja. Od 01 - 31");
                    return false;
                }
                if (field.value > 31) {
                    $("span").text("Dan nije pravilno unesen, mora biti 31 ili manji od 31.");
                    return false;
                }

            }
            //mesec od do
            
            if (field.name == 'mesec-od' || field.name == 'mesec-do') {
                if (field.value.length != 2) {
                    $("span").text("Mesec bi trebao da sadrzi dva broja. Od 01 - 12");
                    return false;
                }
                if (field.value > 12) {
                    $("span").text("Mesec nije pravilno unesen, mora biti 12 ili manji od 12.");
                    return false;
                }
            }

            //godina od do
            
            if (field.name == 'godina-od' || field.name == 'godina-do') {
                if (field.value.length != 4) {
                    $("span").text("Godina bi trebao da sadrzi cetri broja. Od 1990 - xxxx");
                    return false;
                }
                if (field.value < 1990) {
                    $("span").text("Godina nije dobro unesena, mora biti veca od 1990.");
                    return false;
                }
            }

            return true;
        }


        var dateFrom = new Date(datumOd["godina-od"], datumOd["mesec-od"], datumOd["dan-od"]);
        var dateTo = new Date(datumDo["godina-do"], datumDo["mesec-do"], datumDo["dan-do"]);

        if (dateFrom.getTime() > dateTo.getTime()) {
            $("span").text("Datum Od ne moze biti vec nego Do.");
        }
        else {
            $("span").text("Uneli ste datume ispravno.");
        }
    });
});